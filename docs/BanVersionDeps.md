# Ban Dependencies By Version

This is a rule for [Maven Enforcer Plugin](http://maven.apache.org/enforcer/maven-enforcer-plugin/) or The Loving Iron Fist of Maven™

Runs in maven 2 or 3 under maven-enforcer-plugin 1.3.1 or later.

A [Java Regular Expression](http://docs.oracle.com/javase/6/docs/api/java/util/regex/Pattern.html#sum) is specified and any dependencies (including transitive) which have a resolved version that matches will be banned.

## Parameters

**bannedDependencyVersionRegexp** *mandatory* dependencies with version strings that match are banned

**noFailReactorVersionRegexp** *optional* will not fail if artifact being built has a version string that matches

**noFailSnapshots** *optional* will not fail if artifact being built is a snapshot

**ignoreTest** *optional* will not check dependencies with a scope of "test" 

**message** *optional* message to display on finding a banned dependency

**includes** *optional* list of dependency patterns to include when checking

**excludes** *optional* list of dependency patterns to exclude when checking

## Sample Configuration

    <project>
    [...]
        <build>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-enforcer-plugin</artifactId>
                    <version>${maven.enforcer.version}</version>
                    <dependencies>
                        <dependency>
                            <groupId>com.atlassian.maven.enforcer</groupId>
                            <artifactId>maven-enforcer-rules</artifactId>
                            <version>${atlassian.enforcer.rules.version}</version>
                        </dependency>
                    </dependencies>
                    <executions>
                        <execution>
                            <id>ban-milestones-and-release-candidates</id>
                            <goals>
                                <goal>enforce</goal>
                            </goals>
                            <phase>validate</phase>
                            <configuration>
                                <rules>
                                    <banVersionDeps implementation="com.atlassian.maven.enforcer.BanVersionDeps">
                                        <message>Milestone and Release Candidate dependencies are not allowed in releases.</message>
    
                                        <!-- ban dependencies with milestone or release candidate versions -->
                                        <bannedDependencyVersionRegexp>(?i)^.*-(rc|m)-?[0-9]+(-.+)?$</bannedDependencyVersionRegexp>
    
                                        <!-- let them off with a warning if they're building a milestone or snapshot -->
                                        <noFailReactorVersionRegexp>(?i)^.*-m-?[0-9]+(-.+)?$</noFailReactorVersionRegexp>
                                        <noFailSnapshots>true</noFailSnapshots>
    
                                        <!-- only ban direct dependencies -->
                                        <searchTransitive>false</searchTransitive>
                                        
                                        <!-- don't check test scoped dependencies -->
                                        <ignoreTest>true<ignoreTest>

                                        <excludes>
                                            <exclude>com.atlassian.velocity.*</exclude>
                                        </excludes>
                                        <includes>
                                            <include>com.atlassian.*</include>
                                        </includes>
                                    </banVersionDeps>
                                </rules>
                                <fail>true</fail>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </build>
    [...]
    </project>

## Sample Output

    [WARNING] Rule 0: com.atlassian.maven.enforcer.BanVersionDeps failed with message:
                               `                                                      
                            `#####;.`                                                 
                           :##+;;'####'                                               
                          ;#+;;;;;;;+###.                                             
                         ,#;;::::::::;####`                                           
                        .#;;::::::::::;;####                                          
                       ,#;:::::::::::::::'###;                                        
                      ;#:::::::::::::::::::+###`                                      
                     ##:::::::::::::::::::::;####                                     
                    +#:::::::::,,,::::::::::::+###;                                   
                   #'::::::::,,,,,,,,,::::::::;:###+                                  
                  #':::::::,,,,,,,,,,,,::,:::::::'##                                  
                 #;:::::::,,,,,,,,,,,,,,,,,,,,::;+##                                  
               `#;::::,,,,,,,,,,,,,,,,,,,,,,,::;++##                                  
              .#:::::,,,,,,,,,,,,,,,,,,,,,,,,:;+++##`                                 
             :#:::::,,,,,,,,,,,,,,,,...,,,,,,'++++##`                                 
            '+:::::,,,,,,,,,,,,,,,,.....,,,,'+++++##`                                 
           #':::::,,,,,,,,,,,,,,,,.....,,,,'++++++##`                                 
          #':::::,,,,,,,,,,,,,,,,......,,,'+++++++##`                                 
        .#':::::,,,,,,,,,,,,,,,,........,'++++++++##``                                
       :#':::::,,,,,,,,,,,,,,,,........,;+++++++++##.``                               
      ;#;::::,,,,,,,,,,,,,,,,..........;++++++++++##..`                               
      #+:::,,,,,,,,,,,,,,,,,..........'+++++++++++#+,.`                               
     `#:,,,,,,,,,,,,,,,,,,,........,,;+++'''+++++##:,.`                               
     ;#:,,,,,,,,,,,,,,,,,..........,,++''+++++++##;:,``                               
     #+;,,,,,,,,,.....,,...........,++''''''#++##':,.`                                
     #+;;:,,,,,......,............,++'''''''+####;:,`                                 
     +#;;;:,,,...................:+'''''';'''''####'`                                 
     :#;;;;;:,..................:''''''';;;;;''''####+                                
     .#;;::;;;,................,'''''+''';;;;;;;'''####'                              
      #;;:::::::,.............,''''''+''';;;;;;;;;'''####;                            
      #';;::::::::,..........,''''''++'''';;;;;;;;;;'''####:                          
     `##;;;;::::::;;:,......,;''''''++''''''';;;;;;;;;'''+###.                        
     `+#;;;;;::::::;;;;,..,,''''''''++''''''''';;;;;;;;;''''###                       
     `;#;;;;;;:::::;;;;;;:;'''''''''###'''''''''';;;;;;;;;''''##+                     
    `.,+#';;;;;;:;;;;;;;;;;'''''''''+####''''''''';;;;;;;;;;''''+##                   
     `.:##+;;;;;;;;;;;;;;;;'''''''''+######''''''''';;;;;;;;;;''''##+                 
     `.,;###';;;;;;;;;;;;;;''''''''+###+#####''''''';;;;;;;;;;;;''''##'               
     `.,:;+###;;;;;;;;;;;;;''''''''###'';'#####+'''''';;;;;;;;;;;;''''###.            
      `.,:;'####;;;;;;;;;;;+''''''##+';:::;;#####+''''';;;;;;;;;;;;;''''+##,          
       `.,:;'+###+;;;;;;;;;''''''##'':,,,.,::;'#####''''';;;;;;;;;;;;;'''''##         
       ``.,,;;'####';;;;;;;'''''##';:,..```..::;'#####'''''';;;;;;;;;;;;''''+#+       
        ``..,:;'######;;;;;''++##';,,.```````..::;'####+'''''''';;;;;;;;;'''''##`     
          ``..,:;'######+;;'+###';,..``    ````..:;;;####+'''''''';;;;;;;;;'''++#.    
            ``..,:;'##########+';,.``         ```..,:;'####'''''''';;;;;;;;;''+++#    
             ``..,::;'########;;,.``            ``...::;;####'''''''';;;;;;;;'#+##'   
               ``..,,:;;+###;;,,.``                ``..::;'####'''''''';;;;;'+#++#'   
                 ``..,,::;;:,,..``                   ``..::;'###'''''''';;'+++##++#   
                   ```........``                      ```.,:;;+##+'''''''''++#++''#:  
                     `````````                          ```.,::;###'''''''+++#'#+'+#  
                                                          ```.,:;'##+'''''+++#'##;'#' 
                                                            ``..,:;'##+'''++#'+##;'## 
                                                             ```..,:;##++++#+'###;'+# 
                                                               ``..,,:'#####'+###;;+#.
                                                                 `..,,::'##;+##+#;'+#.
                                                                 `.,:'###+'++#+'#;'+#.
                                                            `,+#########'+++#'';';'+#,
                                                          ;#####+'''';'++++#+;:#;''#+.
                                                        `###';;;;;;'++++++#';:'+;''#:.
                                                        ##;;'+++++++++++##';:,#;''+#,`
                                                       ##;;'++++++++++##+;:,.#;''++#,`
                                                      .#';;+++++++####';:,.'#;'''+#:, 
                                                      +#+;;+######+';;:,,'#+;'''+##:. 
                                                      ##+';###++';;:;;####;'''''+#:,  
                                                      ##++';+#########+';''++''##::.  
                                                      ,##+++';;;;;;;;;;'''++++##;:.`  
                                                     ``'##+++++'''''''''''+++##;:,`   
                                                      `.;###++++++++''''++++##;:,`    
                                                      `.,:#####++++++++++###+;:,`     
                                                       `.,:'##############';:,.`      
                                                        `.,,:;;'++####'';:::,`        
                                                         ```..,,::::::,,,...          
                                                            `````......```            
                                                                ```````               
    Milestone and Release Candidate dependencies are not allowed in releases.
    Found Banned Dependency: com.atlassian.plugins:atlassian-plugins-webresource-api:jar:3.0.0-m24
    Found Banned Dependency: com.atlassian.plugins:atlassian-plugins-webfragment:jar:3.0.0-m9
    Found Banned Dependency: com.atlassian.plugins:atlassian-plugins-webresource:jar:3.0.0-m24
    Found Banned Dependency: com.atlassian.oauth:atlassian-oauth-api:jar:1.9.0-m3
    Use 'mvn dependency:tree' to locate the source of the banned dependencies.
    [INFO] ------------------------------------------------------------------------
    [ERROR] BUILD ERROR
    [INFO] ------------------------------------------------------------------------
    [INFO] Some Enforcer rules have failed. Look above for specific messages explaining why the rule failed.
    [INFO] ------------------------------------------------------------------------
