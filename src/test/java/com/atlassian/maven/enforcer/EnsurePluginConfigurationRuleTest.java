package com.atlassian.maven.enforcer;

import org.apache.maven.enforcer.rule.api.EnforcerRuleException;
import org.apache.maven.enforcer.rule.api.EnforcerRuleHelper;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.PluginExecution;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.Xpp3DomBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.StringReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class EnsurePluginConfigurationRuleTest {

    @Mock
    private EnforcerRuleHelper helper;

    @Mock
    private MavenProject project;

    private List<Plugin> plugins = new LinkedList<>();

    @Spy
    private EnsurePluginConfigurationRule.PluginFilter pluginFilter = new EnsurePluginConfigurationRule.PluginFilter();

    @Spy
    private List<EnsurePluginConfigurationRule.Configuration> configurations = new LinkedList<>();

    @InjectMocks
    private final EnsurePluginConfigurationRule rule = new EnsurePluginConfigurationRule();

    @Before
    public void setup() throws Exception {
        when(helper.getComponent(MavenProject.class)).thenReturn(project);
        when(project.getBuildPlugins()).thenReturn(plugins);
    }

    @Test
    public void noConfigurationIsValid() throws EnforcerRuleException {
        pluginFilter.artifactId = "no-config";
        configurations.add(newConfig("//", ".+", "Test"));

        plugins.add(newPlugin("no-config", null));

        rule.execute(helper);
    }

    @Test
    public void noExecutionConfigurationIsValid() throws EnforcerRuleException {
        pluginFilter.artifactId = "no-exec-config";
        configurations.add(newConfig("//", ".+", "Test"));

        plugins.add(newPlugin("no-exec-config", null, newExecution("empty", null)));

        rule.execute(helper);
    }

    @Test
    public void missingConfig() {
        String failMessage = "Should have content";

        pluginFilter.artifactId = "missing-plugin";
        configurations.add(newConfig("//config/param", ".+", failMessage));

        plugins.add(newPlugin("missing-plugin",
                "<config><other-param>Content</other-param></config>"));

        try {
            rule.execute(helper);
        } catch (EnforcerRuleException ex) {
            assertThat(ex.getMessage(), containsString(failMessage));
            return;
        }

        fail("Expected to be invalid");
    }

    @Test
    public void invalidConfig() {
        String failMessage = "Should be numbers";

        pluginFilter.artifactId = "invalid-plugin";
        configurations.add(newConfig("//config/param", "[0-9]+", failMessage));

        plugins.add(newPlugin("invalid-plugin",
                "<config><param>Content</param></config>"));

        try {
            rule.execute(helper);
        } catch (EnforcerRuleException ex) {
            assertThat(ex.getMessage(), containsString(failMessage));
            return;
        }

        fail("Expected to be invalid");
    }

    @Test
    public void invalidExecutionConfig() {
        String failMessage = "Should be numbers";

        pluginFilter.artifactId = "invalid-exec";
        configurations.add(newConfig("//config/param", "[0-9]+", failMessage));

        plugins.add(newPlugin("invalid-exec", null, newExecution("failing",
                "<config><param>Content</param></config>")));

        try {
            rule.execute(helper);
        } catch (EnforcerRuleException ex) {
            assertThat(ex.getMessage(), containsString(failMessage));
            return;
        }

        fail("Expected to be invalid");
    }

    @Test
    public void invalidMissingConfig() {
        String failMessage = "Should be numbers";

        pluginFilter.artifactId = "invalid-missing";
        configurations.add(newConfig("//config/param", "[0-9]+", failMessage));

        plugins.add(newPlugin("invalid-missing", "<config><plugin>Example</plugin></config>",
                newExecution("failing", "<config><exec>Content</exec></config>")));

        try {
            rule.execute(helper);
        } catch (EnforcerRuleException ex) {
            assertThat(ex.getMessage(), containsString(failMessage));
            return;
        }

        fail("Expected to be invalid");
    }

    @Test
    public void validPluginConfig() throws EnforcerRuleException {
        pluginFilter.artifactId = "valid-plugin";
        configurations.add(newConfig("//config/param", "[0-9]+", "Should be numbers"));

        plugins.add(newPlugin("valid-plugin",
                "<config><param>12345</param></config>"));

        rule.execute(helper);
    }

    @Test
    public void validExecutionConfig() throws EnforcerRuleException {
        pluginFilter.artifactId = "valid-exec";
        configurations.add(newConfig("//config/param", "[0-9]+", "Should be numbers"));

        plugins.add(newPlugin("valid-exec", null, newExecution("valid",
                "<config><param>12345</param></config>")));

        rule.execute(helper);
    }

    @Test
    public void validMixedConfigInPlugin() throws EnforcerRuleException {
        pluginFilter.artifactId = "valid-mixed";
        configurations.add(newConfig("//config/param", "[0-9]+", "Should be numbers"));

        plugins.add(newPlugin("valid-mixed", "<config><param>12345</param></config>",
                newExecution("valid", "<config><exec>abcdef</exec></config>")));

        rule.execute(helper);
    }

    @Test
    public void validMixedConfigInExec() throws EnforcerRuleException {
        pluginFilter.artifactId = "valid-mixed";
        configurations.add(newConfig("//config/param", "[0-9]+", "Should be numbers"));

        plugins.add(newPlugin("valid-mixed", "<config><plugin>abcd</plugin></config>",
                newExecution("valid", "<config><param>12345</param></config>")));

        rule.execute(helper);
    }

    @Test
    public void ignoreByArtifactId() throws EnforcerRuleException {
        pluginFilter.artifactId = "to-match";
        configurations.add(newConfig("//config/param", ".+", "Should have content"));

        plugins.add(newPlugin("non-matching",
                "<config><other-param>Content</other-param></config>"));

        rule.execute(helper);
    }

    @Test
    public void ignoreByGroupId() throws EnforcerRuleException {
        pluginFilter.groupId = "non.matching.groupid";
        pluginFilter.artifactId = "matching-artifactId";

        configurations.add(newConfig("//config/param", ".+", "Should have content"));

        plugins.add(newPlugin("matching-artifactId",
                "<config><other-param>Content</other-param></config>"));

        rule.execute(helper);
    }

    @Test(expected = EnforcerRuleException.class)
    public void failOnNoPluginFilter() throws EnforcerRuleException {
        pluginFilter.artifactId = "";
        rule.execute(helper);
    }

    @Test(expected = EnforcerRuleException.class)
    public void failOnNoConfiguration() throws EnforcerRuleException {
        pluginFilter.artifactId = "no-config";
        rule.execute(helper);
    }

    private EnsurePluginConfigurationRule.Configuration newConfig(String xpath, String regexp, String message) {
        EnsurePluginConfigurationRule.Configuration config = new EnsurePluginConfigurationRule.Configuration();
        config.xpath = xpath;
        config.regexp = regexp;
        config.message = message;
        return config;
    }

    private Plugin newPlugin(String artifactId, String configuration, PluginExecution... executions) {
        try {
            Plugin plugin = new Plugin();
            plugin.setGroupId("com.atlassian.test");
            plugin.setArtifactId(artifactId);

            if (configuration != null) {
                plugin.setConfiguration(Xpp3DomBuilder.build(new StringReader(configuration)));
            }

            plugin.setExecutions(Arrays.asList(executions));

            return plugin;
        } catch (Exception e) {
            throw new RuntimeException("Failed to create Plugin stub", e);
        }
    }

    private PluginExecution newExecution(String id, String configuration) {
        try {
            PluginExecution execution = new PluginExecution();
            execution.setId(id);

            if (configuration != null) {
                execution.setConfiguration(Xpp3DomBuilder.build(new StringReader(configuration)));
            }

            return execution;
        } catch (Exception e) {
            throw new RuntimeException("Failed to create Plugin stub", e);
        }
    }

}