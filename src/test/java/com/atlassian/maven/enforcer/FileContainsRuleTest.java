package com.atlassian.maven.enforcer;

import org.apache.maven.enforcer.rule.api.EnforcerRuleException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.net.URISyntaxException;

public class FileContainsRuleTest
{
    private FileContainsRule fileContainsRule;
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp()
    {
        fileContainsRule = new FileContainsRule();
    }

    @After
    public void tearDown()
    {
        fileContainsRule = null;
    }

    @Test
    public void fileConfigRequired() throws Exception
    {
        fileContainsRule.setContainsString("banana bread");

        exception.expect(EnforcerRuleException.class);
        exception.expectMessage("A <file> must be specified in the configuration.");

        fileContainsRule.execute(null);
    }

    @Test
    public void fileMustExist() throws Exception
    {
        final File file = new File("this file does not exist");
        fileContainsRule.setFile(file);
        fileContainsRule.setContainsString("banana bread");

        exception.expect(EnforcerRuleException.class);
        exception.expectMessage("File not found: " + file);

        fileContainsRule.execute(null);
    }

    @Test
    public void fileMatchesString() throws Exception
    {
        final File file = getTestResource("com/atlassian/maven/enforcer/fox.txt");
        fileContainsRule.setFile(file);
        fileContainsRule.setContainsString("brown fox");

        fileContainsRule.execute(null);
    }

    @Test
    public void fileDoesNotMatchString() throws Exception
    {
        final File file = getTestResource("com/atlassian/maven/enforcer/fox.txt");
        fileContainsRule.setFile(file);
        fileContainsRule.setContainsString("banana bread");

        exception.expect(EnforcerRuleException.class);
        exception.expectMessage("Pattern \\Qbanana bread\\E was not found in file " + file);

        fileContainsRule.execute(null);
    }

    @Test
    public void fileMatchesPattern() throws Exception
    {
        final File file = getTestResource("com/atlassian/maven/enforcer/fox.txt");
        fileContainsRule.setFile(file);
        fileContainsRule.setContainsPattern("(brown|red)\\s*fox");

        fileContainsRule.execute(null);
    }

    @Test
    public void fileDoesNotMatchPattern() throws Exception
    {
        final File file = getTestResource("com/atlassian/maven/enforcer/fox.txt");
        fileContainsRule.setFile(file);
        fileContainsRule.setContainsPattern("(black|white)\\s*fox");

        exception.expect(EnforcerRuleException.class);
        exception.expectMessage("Pattern (black|white)\\s*fox was not found in file " + file);

        fileContainsRule.execute(null);
    }

    private static File getTestResource(final String path) throws URISyntaxException
    {
        return new File(FileContainsRuleTest.class.getResource("/" + path).toURI());
    }
}
